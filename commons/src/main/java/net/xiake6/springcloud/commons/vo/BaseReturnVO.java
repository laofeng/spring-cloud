package net.xiake6.springcloud.commons.vo;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

/**
 * 
 * @author fenglibin
 *
 */
@Data
@Builder
public class BaseReturnVO implements Serializable {
	private static final long serialVersionUID = 9150713445164132558L;
	private int result; // 结果码
	private String msg; // 结果描述
	private Object data;

	@Tolerate
	public BaseReturnVO() {

	}

	public static BaseReturnVO success() {
		return BaseReturnVO.builder().result(0).msg("success").data(null).build();
	}

	public static BaseReturnVO success(String msg) {
		return BaseReturnVO.builder().result(0).msg(msg).data(null).build();
	}

	public static BaseReturnVO success(Object data) {
		return BaseReturnVO.builder().result(0).msg("success").data(data).build();
	}

	public static BaseReturnVO success(String msg, Object data) {
		return BaseReturnVO.builder().result(0).msg(msg).data(data).build();
	}

	public static BaseReturnVO failure() {
		return BaseReturnVO.builder().result(-1).msg("fail").data(null).build();
	}

	public static BaseReturnVO failure(String msg) {
		return BaseReturnVO.builder().result(-1).msg(msg).data(null).build();
	}

	public static BaseReturnVO failure(Object data) {
		return BaseReturnVO.builder().result(-1).msg("fail").data(data).build();
	}

	public static BaseReturnVO failure(String msg, Object data) {
		return BaseReturnVO.builder().result(-1).msg(msg).data(data).build();
	}

	public static BaseReturnVO failure(int result, String msg, Object data) {
		return BaseReturnVO.builder().result(result).msg(msg).data(data).build();
	}
}
