/*
 * Copyright (c) 2017 sunvalley. All Rights Reserved.
 * @author fenglibin
 * @date  2019-08-26 4:39 PM
 */
package net.xiake6.springcloud.gateway.common;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import java.util.Enumeration;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * 通过feign调用下游服务的时候处理 header
 * 当前未启用
 * @author fenglibin
 * @date 2019-08-26 4:39 PM
 */
//@Configuration
public class FeignConfiguration implements RequestInterceptor {
  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Override
  public void apply(RequestTemplate template) {
    ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder
        .getRequestAttributes();
    HttpServletRequest request = attributes.getRequest();
    Enumeration<String> headerNames = request.getHeaderNames();
    if (headerNames != null) {
      while (headerNames.hasMoreElements()) {
        String name = headerNames.nextElement();
        String values = request.getHeader(name);
        template.header(name, values);

      }
      logger.info("feign interceptor header:{}", template);
    }
  }
}

