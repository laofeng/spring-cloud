package net.xiake6.springcloud.application2.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

/**
 * 配置项属性文件
 * 
 * @author fenglibin
 */

@ConfigurationProperties(prefix = "test.keys")
@Data
public class ConfigProperties {

	// 当前服务器的ＩＰ地址
	private String key1 = null;
	// 当前服务器的名称
	private String key2 = null;
}