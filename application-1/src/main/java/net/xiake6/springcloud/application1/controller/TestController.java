package net.xiake6.springcloud.application1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import net.xiake6.springcloud.application1.config.ConfigProperties;
import net.xiake6.springcloud.application1.service.remote.Application2Service;
import net.xiake6.springcloud.commons.vo.BaseReturnVO;

/**
 * 
 * @author fenglibin
 *
 */
@RestController
@RequestMapping("/test")
@Slf4j
public class TestController {
	
	@Autowired
	private ConfigProperties configProperties;
	
	@Autowired
	private Application2Service application2Service;

    @GetMapping("/getValueOfKey1")
    public BaseReturnVO getValueOfKey1() {
        log.info("getValueOfKey1 request is coming...");
        return BaseReturnVO.success(configProperties.getKey1());
    }
    
    @GetMapping("/getARandomValue")
    public BaseReturnVO getARandomValue() {
        log.info("Request is coming...");
        return BaseReturnVO.success(application2Service.getARandomValue().getData());
    }

}
