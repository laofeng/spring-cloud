/*
 * Copyright (c) xiake6. All Rights Reserved.
 * @author fenglibin
 * @date  2019-05-30 16:36
 */
package net.xiake6.springcloud.gateway.vo;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 类或方法的功能描述 :TODO
 *
 * @author fenglibino
 * @date 2019-05-30 16:36
 */
public class UserLoginVO {
  /**
   * 用户ID。
   */
  private Integer userId;

  /**
   * token.
   */
  private String accessToken;

  /**
   * 是否有权限.
   */
  private Boolean permission;

  public Boolean getPermission() {
    return permission;
  }

  public void setPermission(Boolean permission) {
    this.permission = permission;
  }

  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this,
        ToStringStyle.MULTI_LINE_STYLE)
        .append("userId", userId)
        .append("accessToken", accessToken)
        .append("permission", permission)
        .toString();
  }
}

