/*
 * Copyright (c) xiake6. All Rights Reserved.
 * @owner: fenglibin
 * @date:  2017-11-18 17:38
 */
package net.xiake6.springcloud.gateway.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.Strings;

import net.xiake6.springcloud.commons.enums.ApiMsgEnum;
import net.xiake6.springcloud.commons.vo.BaseReturnVO;
import net.xiake6.springcloud.gateway.filter.PostFilter;
import net.xiake6.springcloud.gateway.properties.GatewayProperties;
import net.xiake6.springcloud.gateway.remote.IAuthService;

/**
 * 类或方法的功能描述 :TODO
 *
 * @author: fenglibin
 * @date: 2017-11-18 17:38
 */
@Service
public class AuthService {

  private static Logger logger = LoggerFactory.getLogger(PostFilter.class);

  @Autowired
  private GatewayProperties gatewayProperties;

  @Autowired
  private IAuthService iAuthService;


  private static Map<String, String> serverNameRoutes = new HashMap<>();


  public BaseReturnVO verifyToken(String accessToken,String path, String requestUrl,String userAgent){
    if(Strings.isNullOrEmpty(accessToken)){
      return BaseReturnVO.failure(ApiMsgEnum.LOGIN_FAIL.getResDes());
    }
    return  iAuthService.verifyToken(accessToken,path,requestUrl,userAgent);
  }


  public boolean checkWhitePaths(String path) {
    if(Strings.isNullOrEmpty(path)){
      return false;
    }
    Optional<String> hasUrl = gatewayProperties.getWhitePaths().stream().filter(url -> path.matches(url)).findAny();
    if (hasUrl.isPresent()) {
      return true;
    }
    return false;
  }

}

