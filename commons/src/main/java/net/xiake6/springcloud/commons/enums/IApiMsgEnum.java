/*
 * Copyright (c) 2014 xiake6. All Rights Reserved.
 * @author fenglibin
 * @date  2019-06-05 16:43
 */
package net.xiake6.springcloud.commons.enums;

/**
 * 类或方法的功能描述 :TODO
 *
 * @author fenglibino
 * @date 2019-06-05 16:43
 */
public interface IApiMsgEnum {

  /**
   * 获取code.
   * @return
   */
  int getResCode();


  /**
   * 获取消息体.
   * @return
   */
  String getResDes();

}

