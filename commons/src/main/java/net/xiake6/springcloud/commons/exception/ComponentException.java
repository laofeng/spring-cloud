/*
* Copyright (c) 2016 xiake6. All Rights Reserved.
*/
package net.xiake6.springcloud.commons.exception;

import net.xiake6.springcloud.commons.enums.IApiMsgEnum;

/**
 * 组件异常。
 * 主要调用第三方接口返回的异常.
 *
 * @author: fenglibin
 * @date: 2017-09-06 19:32
 */
public class ComponentException  extends RuntimeException implements BaseException {
    /** serialVersionUID */
    private static final long serialVersionUID = 2332608236621015982L;
    private int code;
    private Throwable throwable;

    public ComponentException() {
        super();
    }

    public ComponentException(String message) {
        super(message);
    }

    public ComponentException(int code, String message) {
        super(message);
        this.code = code;
    }

    public ComponentException(Throwable cause) {
        super(cause);
        this.throwable=cause;
    }

    public ComponentException(String message, Throwable cause) {
        super(message, cause);
        this.throwable=cause;
    }

    public ComponentException(int code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
        this.throwable=cause;
    }


    public ComponentException(IApiMsgEnum apiMsgEnum, String message) {
        super(message);
        this.code = apiMsgEnum.getResCode();
    }

    public ComponentException(IApiMsgEnum apiMsgEnum) {
        super(apiMsgEnum.getResDes());
        this.code = apiMsgEnum.getResCode();
    }


    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public Throwable getThrowable() {
        return throwable;
    }
}

