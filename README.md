### 服务说明

| 服务名              | 端口  | 备注                                                         |
| ------------------- | ----- | ------------------------------------------------------------ |
| commons             |       | 公共服务包，提供公共方法及公共的ＶＯ等，供eureka-service、config-service、gateway-service、application-1、application-2信赖引用。 |
| eureka-service      | 8761  | Eureka服务                                                   |
| config-service      | 8763  | Git配置中心服务                                              |
| gateway-service     | 8762  | ZUUL网关服务                                                 |
| application-1       | 20250 | 示例应用１，其会通过Feign调用示例应用２提供的接口            |
| application-2       | 20260 | 示例应用２                                                   |
| spring-cloud-config |       |                                                              |


### 项目搭建及运行

１、config-service本地的Git缓存目录默认为/data/spring-cloud-config/repo，如果该目录不存在则需要建立该目录；config-service是从git中去拉取配置的，如果没有现成的则需要先搭建一个gitlab，我建意通过docker搭建，比较方便，以下是一个操作命令：

| Docker安装Gitlab                                             |
| ------------------------------------------------------------ |
| １）拉取镜像：<br/>docker pull gitlab/gitlab-ce<br/>２）启动：<br/>docker run -d -p 443:443 -p 8881:80 -p 222:22 --name gitlab --restart always -v /data/gitlab/config:/etc/gitlab -v /data/gitlab/logs:/var/log/gitlab -v /data/gitlab/data:/var/opt/gitlab gitlab/gitlab-ce<br/>３）通过admin/admin登陆后，再创建用户test_user并设置密码test_user，再通过test_user登陆创建名为spring-cloud-config的项目，再把当前目录spring-cloud-config中的内容复制过去并提交到Git中。 |

２、将commons工程安装到本地maven仓库中：mvn source:jar install ；

３、项目启动顺序：eureka-service、 config-service、gateway-service、application-1、application-2；

４、通过Eureka查过各服务及应用的启动情况，访问ＵＲＬ：http://127.0.0.1:8761/　；

５、验证：

​	访问application-1提供的接口：

​		http://127.0.0.1:20250/test/getARandomValue  该接口实际会访问application-2提供的接口

​		http://127.0.0.1:20250/test/getValueOfKey1

​	访问application-2提供的接口：http://127.0.0.1:20260/test/getARandomValue

​	访问网关接口：

​		http://127.0.0.1:8762/api/test/getValueOfKey1

​		http://127.0.0.1:8762/api/test/getARandomValue

​	如果以上都可以正常访问，则说明应用启动成功
