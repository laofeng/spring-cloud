/*
 * Copyright (c) xiake6. All Rights Reserved.
 * @owner: fenglibin
 * @date:  2018-01-26 16:46
 */
package net.xiake6.springcloud.gateway.remote;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import net.xiake6.springcloud.commons.vo.BaseReturnVO;

/**
 * 用户服务接口
 *
 * @author: fenglibin
 * @date: 2018-01-26 16:46
 */
@FeignClient(name = "erp-user")
public interface IAuthService {

    @RequestMapping(value = "/apiauth/hasPermiss", method = RequestMethod.GET)
    BaseReturnVO verifyToken(@RequestParam("access_token") String accessToken, @RequestParam("apiPath") String apiPath,
            @RequestParam("requestUrl") String requestUrl, @RequestParam("userAgent") String userAgent);
}

