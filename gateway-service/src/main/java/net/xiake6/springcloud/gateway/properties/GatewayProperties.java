/*
* Copyright (c) xiake6. All Rights Reserved.
* @owner: fenglibin
* @date:  2018-04-28 10:54
*/
package net.xiake6.springcloud.gateway.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 类或方法的功能描述 :TODO
 *
 * @author: fenglibin
 * @date: 2018-04-28 10:54
 */

@RefreshScope
@Component
@ConfigurationProperties(prefix = "zuul")
public class GatewayProperties {
    @Value("${white-paths:}")
    private List<String> whitePaths;  //白名单

//    @ApolloConfigChangeListener(value = "application.yml")
//    private void onChange(ConfigChangeEvent changeEvent) {
//
//        for (String key : changeEvent.changedKeys()) {
//            ConfigChange change = changeEvent.getChange(key);
//            //logger.info("Found change - {}", change.toString());
//        }
//
//        if (changeEvent.isChanged("request.timeout")) {
//            //refreshTimeout();
//        }
//    }

    public List<String> getWhitePaths() {
        return whitePaths;
    }

    public void setWhitePaths(List<String> whitePaths) {
        this.whitePaths = whitePaths;
    }
}

