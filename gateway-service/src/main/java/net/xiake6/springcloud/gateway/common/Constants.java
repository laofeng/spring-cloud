/*
 * Copyright (c) xiake6. All Rights Reserved.
 * @author jiang chao
 * @date  2019-06-11 18:47
 */
package net.xiake6.springcloud.gateway.common;

/**
 * 常量类.
 *
 * @author jiang chao
 * @date 2019-06-11 18:47
 */
public final class Constants {

  public static final String  HEADER_USER_KEY = "x-erp-auth-id";

  public static final String  HEADER_TOKEN_KEY ="Authorization";

  public static final String  QUERY_TOKEN_KEY="access_token";

  public static final String  COOKIE_TONEK_KEY="noah_platform_token";

  public final static String LOG_SPLITER = "^_^";

  public final static  String  Expose_Headers ="Access-Control-Expose-Headers";

  public final static  String  TRACE_ID ="traceId";

  public final static  String  RESPONSE_STATUS_CODE= "responseStatusCode";

  public final static String  RESPONSE_BODY = "responseBody";

  public final static String   REQUEST_TIME ="requestTime";

  public final static String RIBBON_RESPONSE = "ribbonResponse";

  public final static String  SERVICE_ID = "serviceId";

  public final static String   REQUEST_USER_AGENT ="user-agent";

  public final static String REQUEST_REFERER ="Referer";





}

