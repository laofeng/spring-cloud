package net.xiake6.springcloud.eureka.config;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * 添加eureka httpbase身份验证.
 *
 * @author fenglibin
 * @date 2019-04-22 10:07
 */
@Configuration
@EnableWebSecurity
public class EurekaSecurityConfiguration extends WebSecurityConfigurerAdapter {
  private static final String DISCOVERY_REALM = "API  Discovery Service realm";

  @Value("${eureka.security.enabled:true}")
  private boolean  security;

  @Value("${eureka.security.user.name:eurekaCli}")
  private String eurekaUser;

  @Value("${eureka.security.user.password:123456}")
  private String eurekaPassword;

  @Value("${eureka.security.ssl:false}")
  private boolean verifySslCertificatesOfServices;

  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
    auth
        .inMemoryAuthentication().passwordEncoder(encoder)
        //eureka-security-client
        .withUser(eurekaUser).password(encoder.encode(eurekaPassword)).roles("EUREKA-CLIENT");
    ;
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    //禁用跨域
    http.csrf().disable();
    http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    http.headers().httpStrictTransportSecurity().disable();

    if(security){
      //注意： http://${user}:${password}@${host}:${port}/eureka/ 这种方式登录,必须是httpBasic
      if (verifySslCertificatesOfServices) {
        http.antMatcher("/**").authorizeRequests().anyRequest().authenticated().and().x509()
            .userDetailsService(x509UserDetailsService()).and().httpBasic();
      } else {
        //所有路径需要验证 /eureka/**"
        http.httpBasic().realmName(DISCOVERY_REALM).and().antMatcher("/**").authorizeRequests()
            .anyRequest().authenticated();
      }
    }

  }


  public UserDetailsService x509UserDetailsService() {

    return new UserDetailsService() {
      public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        List<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();
          authorities.add(new SimpleGrantedAuthority("EUREKA-CLIENT"));
        return new User(eurekaUser, eurekaPassword, authorities);
      }
    };
  }
}

