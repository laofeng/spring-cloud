package net.xiake6.springcloud.application2.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import net.xiake6.springcloud.commons.vo.BaseReturnVO;

/**
 * 
 * @author fenglibin
 *
 */
@RestController
@RequestMapping("/test")
@Slf4j
public class TestController {

    @GetMapping("/getARandomValue")
    public BaseReturnVO getARandomValue() {
        log.info("getARandomValue request is coming...");
        return BaseReturnVO.success((int)(Math.random()*100000));
    }

}
