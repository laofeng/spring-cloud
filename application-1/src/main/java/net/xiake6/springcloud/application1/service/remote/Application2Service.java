package net.xiake6.springcloud.application1.service.remote;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import net.xiake6.springcloud.commons.vo.BaseReturnVO;


@FeignClient(value = "application-2")
public interface Application2Service {
	@RequestMapping(value = "/test/getARandomValue",method = RequestMethod.GET)
	public BaseReturnVO getARandomValue();
}
