package net.xiake6.springcloud.gateway;

import net.xiake6.springcloud.gateway.filter.AccessFilter;
import net.xiake6.springcloud.gateway.filter.PostFilter;
import net.xiake6.springcloud.gateway.gateway.ApiExceptionHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;


/**
 * GatewayApplication.
 * erp-data 上传下载接口不加入熔断超时处理.
 * 新加服务需要在这里申明@bean，当服务不可用才会触发503
 *
 * @author: fenglibin
 * @date: 2017-10-16 15:25
 */

@EnableEurekaClient
@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
@EnableZuulProxy
@EnableFeignClients
public class ServiceApplication {

	private static Logger logger = LoggerFactory.getLogger(ServiceApplication.class);

	public static void main(String[] args) {

		SpringApplication.run(ServiceApplication.class, args);

		logger.info("server start");
	}


	@Bean
	public AccessFilter accessFilter() {
		return new AccessFilter();
	}


	@Bean
	public PostFilter postFilter(){
		return new PostFilter();
	}

}
