/*
 * Copyright (c) xiake6. All Rights Reserved.
 * @author jiang chao
 * @date  2019-06-15 10:32
 */
package net.xiake6.springcloud.gateway.vo;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 类或方法的功能描述 :TODO
 *
 * @author jiang chao
 * @date 2019-06-15 10:32
 */
public class PermissVO {

  private String msg;
  private Integer result;
  private Integer isPower;
  private Integer isLogOut;

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public Integer getResult() {
    return result;
  }

  public void setResult(Integer result) {
    this.result = result;
  }

  public Integer getIsPower() {
    return isPower;
  }

  public void setIsPower(Integer isPower) {
    this.isPower = isPower;
  }

  public Integer getIsLogOut() {
    return isLogOut;
  }

  public void setIsLogOut(Integer isLogOut) {
    this.isLogOut = isLogOut;
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this,
        ToStringStyle.MULTI_LINE_STYLE)
        .append("msg", msg)
        .append("result", result)
        .append("isPower", isPower)
        .append("isLogOut", isLogOut)
        .toString();
  }
}

