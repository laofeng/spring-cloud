### 端口分配

| 服务名          | 端口  | 备注     |
| --------------- | ----- | -------- |
| eureka-service  | 8761  | 系统服务 |
| gateway-service | 8762  | 系统服务 |
| config-service  | 8763  | 系统服务 |
| application-1   | 20250 |          |
| application-2   | 20260 |          |
注：config-service本地的Git缓存目录默认为/data/spring-cloud-config/repo，如果该目录不存在则需要建立该目录
