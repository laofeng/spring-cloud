package net.xiake6.springcloud.application1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;


/**
 * "@SpringBootApplication"这个注解和 同时使用 “@EnableAutoConfiguration” 和“@ComponentScan”效果一样<br/>
 * "@ServletComponentScan" 启动时spring能够扫描到我们自己编写的servlet和filter
 *
 * @author fenglibin
 */
@SpringBootApplication(scanBasePackages={"net.xiake6.springcloud"})
@EnableFeignClients
@EnableEurekaClient
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
