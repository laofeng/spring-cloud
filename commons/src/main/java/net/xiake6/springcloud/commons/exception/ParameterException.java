/**
 * @(#)ParameterException.java 2011-12-20 Copyright 2011 it.kedacom.com, Inc.
 *                             All rights reserved.
 */

package net.xiake6.springcloud.commons.exception;

import net.xiake6.springcloud.commons.enums.IApiMsgEnum;

/**
 * 参数异常类
 */
public class ParameterException extends RuntimeException implements BaseException {

	/** serialVersionUID */
	private static final long serialVersionUID = 2332608236621015982L;
	private int code;
	private Throwable throwable;

	public ParameterException() {
		super();
	}

	public ParameterException(String message) {
		super(message);
	}

	public ParameterException(int code, String message) {
		super(message);
		this.code = code;
	}

	public ParameterException(Throwable cause) {
		super(cause);
		this.throwable=cause;
	}

	public ParameterException(String message, Throwable cause) {
		super(message, cause);
		this.throwable=cause;
	}

	public ParameterException(int code, String message, Throwable cause) {
		super(message, cause);
		this.code = code;
		this.throwable=cause;
	}


	public ParameterException(IApiMsgEnum apiMsgEnum, String message) {
		super(message);
		this.code = apiMsgEnum.getResCode();
	}

	public ParameterException(IApiMsgEnum apiMsgEnum) {
		super(apiMsgEnum.getResDes());
		this.code = apiMsgEnum.getResCode();
	}


	public void setCode(int code) {
		this.code = code;
	}

	@Override
	public int getCode() {
		return code;
	}

	@Override
	public Throwable getThrowable() {
		return throwable;
	}
}
