/*
* Copyright (c) xiake6. All Rights Reserved.
* @owner: fenglibin
* @date:  2018-02-12 11:30
*/
package net.xiake6.springcloud.gateway.gateway;

import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.List;

/**
 * 类或方法的功能描述 :TODO
 *
 * @author: fenglibin
 * @date: 2018-02-12 11:30
 */
public class ApiDefaultErrorMessage {

    private int resCode;
    private String resDes;
    private List<String> errors;

    public int getResCode() {
        return resCode;
    }

    public void setResCode(int resCode) {
        this.resCode = resCode;
    }

    public String getResDes() {
        return resDes;
    }

    public void setResDes(String resDes) {
        this.resDes = resDes;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public ApiDefaultErrorMessage () {
    }


    public ApiDefaultErrorMessage (HttpStatus status, String message, List<String> errors) {
        super();
        this.resCode = status.value();
        this.resDes = message;
        this.errors = errors;
    }

    public ApiDefaultErrorMessage (HttpStatus status, String message, String error) {
        super();
        this.resCode = status.value();
        this.resDes = message;
        errors = Arrays.asList(error);
    }
}

