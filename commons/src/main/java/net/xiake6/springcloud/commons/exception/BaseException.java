/*
* Copyright (c) 2016 xiake6. All Rights Reserved.
*/
package net.xiake6.springcloud.commons.exception;

/**
 * 异常基类.
 *
 * @Author:
 * @Date: 2017-01-06 20:08
 */
public interface BaseException {
     int getCode();
     String getMessage();
     Throwable  getThrowable();
}

