package net.xiake6.springcloud.application1.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties({ ConfigProperties.class })
public class ApplicationConfig {

}
