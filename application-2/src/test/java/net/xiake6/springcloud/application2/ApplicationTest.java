package net.xiake6.springcloud.application2;


import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import net.xiake6.springcloud.application2.Application;


/**
 * @Author 
 * @Created in 2018/5/9 13:44
 * @Description:
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@ContextConfiguration
public class ApplicationTest {

   



}